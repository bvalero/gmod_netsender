#ifndef CLIENT_HPP
#define CLIENT_HPP

#include <cstddef>

namespace sdk
{

    namespace client_ns
    {

        enum class FrameStage
        {

            UNDEFINED = -1,
            START,
            NET_UPDATE_START,
            NET_POST_DATA_UPDATE_START,
            NET_POST_DATA_UPDATE_END,
            NET_UPDATE_END,
            RENDER_START,
            RENDER_END

        };

        namespace vft::function
        {

            namespace index
            {

                inline constexpr std::size_t NotifyFrameStage { 35 };

            }

            namespace type
            {

                using NotifyFrameStage = void (__thiscall*) (void* _this, int frameStage);

            }

        }

    }

    class Client
    {

    };

    inline Client* client { nullptr };

}

#endif //CLIENT_HPP
