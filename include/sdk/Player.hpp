#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "util/VirtualFunctionGetter.hpp"

#include "Entity.hpp"

namespace sdk
{

    namespace player
    {

        // Spectator Movement modes
        enum {
            OBS_MODE_NONE = 0,	// not in spectator mode
            OBS_MODE_DEATHCAM,	// special mode for death cam animation
            OBS_MODE_FREEZECAM,	// zooms to a target, and freeze-frames on them
            OBS_MODE_FIXED,		// view from a fixed camera position
            OBS_MODE_IN_EYE,	// follow a player in first person view
            OBS_MODE_CHASE,		// follow a player in third person view
            OBS_MODE_ROAMING,	// free roaming

            NUM_OBSERVER_MODES,
        };

        enum class ObserverMode
        {

            NONE = 0,
            DEATHCAM,
            FREEZECAM,
            FIXED,
            IN_EYE,
            CHASE,
            ROAMING,
            NUM_OBSERVER_MODES

        };

        namespace vft::function
        {

            namespace index
            {

                inline constexpr std::size_t GetObserverMode { 293 };

            }

            namespace type
            {

                using GetObserverMode = int (__thiscall*) (void* _this);

            }

        }

    }

    class Player : public Entity
    {

    public:

        int GetObserverMode()
        {
            return util::GetVirtualFunction<player::vft::function::type::GetObserverMode>(this, player::vft::function::index::GetObserverMode) (this);
        }

    };

}

#endif //PLAYER_HPP
