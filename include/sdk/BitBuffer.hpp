#ifndef BITBUFFER_HPP
#define BITBUFFER_HPP

// unoptimized and unsafe

namespace sdk
{

    namespace util::bitbuffer
    {

        inline unsigned int SwapUInt32Endian(unsigned int uint32)
        {
            return (((uint32 & 0x000000FF) << 24) | ((uint32 & 0x0000FF00) << 8) | ((uint32 & 0x00FF0000) >> 8) | ((uint32 & 0xFF000000) >> 24));
        }

        inline bool IsBigEndian()
        {
            int test { 1 };
            return (*reinterpret_cast<char*>(&test) == 0);
        }

        inline unsigned int SwapUInt32EndianIfBigEndian(unsigned int uint32)
        {
            return IsBigEndian() ? SwapUInt32Endian(uint32) : uint32;
        }

    }

    class BitBufferWriter
    {

    public:
        unsigned int* littleEndianUInt32Buffer { nullptr };
        int maxBytes { 0 };
        int maxBits { 0 };
        int position { 0 };
        bool overflow { false };
        bool assertOnOverflow { false };
        const char* debugName { nullptr };

    public:

        BitBufferWriter()
        {

        }

        BitBufferWriter(unsigned int* littleEndianUInt32Buffer) : littleEndianUInt32Buffer { littleEndianUInt32Buffer }
        {

        }

        void WriteUInt(unsigned int uint, int numberOfBits)
        {
            int littleEndianUInt32BitPosition {this->position & 31 };
            int littleEndianUInt32BufferIndex { this->position >> 5 };
            this->position += numberOfBits;

            unsigned int* littleEndianUInt32 { &this->littleEndianUInt32Buffer[littleEndianUInt32BufferIndex] };

            uint = (uint << littleEndianUInt32BitPosition) | (uint >> (32 - littleEndianUInt32BitPosition));

            unsigned int temp { static_cast<unsigned int>(1 << (numberOfBits - 1)) };
            unsigned int mask { (temp * 2 - 1) << littleEndianUInt32BitPosition };
            unsigned int nextMask { (temp - 1) >> (31 - littleEndianUInt32BitPosition) };

            int nextLittleEndianUInt32Index { static_cast<int>(nextMask & 1) };
            unsigned int nativeEndianUInt32 { util::bitbuffer::SwapUInt32EndianIfBigEndian(littleEndianUInt32[0]) };
            unsigned int nextNativeEndianUInt32 { util::bitbuffer::SwapUInt32EndianIfBigEndian(littleEndianUInt32[nextLittleEndianUInt32Index]) };

            nativeEndianUInt32 ^= (mask & (uint ^ nativeEndianUInt32));
            nextNativeEndianUInt32 ^= (nextMask & (uint ^ nextNativeEndianUInt32));

            littleEndianUInt32[nextLittleEndianUInt32Index] = util::bitbuffer::SwapUInt32EndianIfBigEndian(nextNativeEndianUInt32);
            littleEndianUInt32[0] = util::bitbuffer::SwapUInt32EndianIfBigEndian(nativeEndianUInt32);
        }

        void WriteBits(unsigned char* byteBuffer, int numberOfBits)
        {
            int numberOfBitsLeft { numberOfBits };

            while (numberOfBitsLeft >= 8)
            {
                this->WriteUInt(*byteBuffer, 8);
                ++byteBuffer;
                numberOfBitsLeft -= 8;
            }

            if (numberOfBitsLeft)
            {
                this->WriteUInt(*byteBuffer, numberOfBitsLeft);
            }
        }

        void SetPosition(int position)
        {
            this->position = position;
        }

    };

}

#endif //BITBUFFER_HPP
