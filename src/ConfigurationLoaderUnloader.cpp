#include "ConfigurationLoaderUnloader.hpp"

#include <cstdlib>
#include <cstdint>

#include <string>
#include <unordered_map>
#include <fstream>
#include <filesystem>
#include <optional>

#include <json/json.hpp>

#include <sdk/BitBuffer.hpp>
#include <sdk/Input.hpp>
#include <sdk/NetChannel.hpp>
#include <sdk/NetworkStringTable.hpp>

#include "Configuration.hpp"

constexpr char CONFIGURATION_FILE_HOME_ENVIRONMENT_VARIABLE_NAME[] = "gmod_timer_configuration_file_home";

constexpr char CONFIGURATION_FILE_NAME[] = "configuration";
constexpr char CONFIGURATION_FILE_EXTENSION[] = ".json";
constexpr char TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_DATA_FILE_EXTENSION[] = ".dat";

constexpr char JSON_CONFIGURATION_RELOAD_KEYS_KEY[] = "reloadKeys";

constexpr char JSON_CONFIGURATION_TIMER_CONFIGURATIONS_CONFIGURATIONS_KEY[] = "timerConfigurationsConfigurations";
constexpr char JSON_CONFIGURATION_TIMER_CONFIGURATION_CONFIGURATION_ACTIVATE_KEY[] = "activate";
constexpr char JSON_CONFIGURATION_TIMER_CONFIGURATION_CONFIGURATION_START_KEYS_KEY[] = "startKeys";
constexpr char JSON_CONFIGURATION_TIMER_CONFIGURATION_CONFIGURATION_STOP_KEYS_KEY[] = "stopKeys";
constexpr char JSON_CONFIGURATION_TIMER_CONFIGURATION_CONFIGURATION_LOOP_KEY[] = "loop";
constexpr char JSON_CONFIGURATION_TIMER_CONFIGURATION_CONFIGURATION_AUTOMATIC_START_KEY[] = "automaticStart";

constexpr char JSON_TIMER_CONFIGURATION_TIME_TYPE_KEY[] = "timeType";
constexpr char JSON_TIMER_CONFIGURATION_EXECUTE_IF_DEAD_KEY[] = "executeIfDead";
constexpr char JSON_TIMER_CONFIGURATION_EXECUTE_IF_OBSERVER_KEY[] = "executeIfObserver";

constexpr char JSON_TIMER_CONFIGURATION_EXECUTIONS_KEY[] = "executions";

constexpr char JSON_TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_KEY[] = "netMessage";
constexpr char JSON_TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_NAME_KEY[] = "name";
constexpr char JSON_TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_RELIABLE_KEY[] = "reliable";
constexpr char JSON_TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_DATA_FILE_NAME_KEY[] = "dataFileName";
constexpr char JSON_TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_DATA_BITS_KEY[] = "dataBits";

constexpr char JSON_TIMER_CONFIGURATION_EXECUTION_COMMAND_KEY[] = "command";
constexpr char JSON_TIMER_CONFIGURATION_EXECUTION_NEXT_EXECUTION_DELAY_KEY[] = "nextExecutionDelay";

const std::unordered_map<std::string, sdk::input_ns::KeyCode> keyCodeNamesToValues =
{
    { "KEY_0", sdk::input_ns::KEY_0 },
    { "KEY_1", sdk::input_ns::KEY_1 },
    { "KEY_2", sdk::input_ns::KEY_2 },
    { "KEY_3", sdk::input_ns::KEY_3 },
    { "KEY_4", sdk::input_ns::KEY_4 },
    { "KEY_5", sdk::input_ns::KEY_5 },
    { "KEY_6", sdk::input_ns::KEY_6 },
    { "KEY_7", sdk::input_ns::KEY_7 },
    { "KEY_8", sdk::input_ns::KEY_8 },
    { "KEY_9", sdk::input_ns::KEY_9 },
    { "KEY_A", sdk::input_ns::KEY_A },
    { "KEY_B", sdk::input_ns::KEY_B },
    { "KEY_C", sdk::input_ns::KEY_C },
    { "KEY_D", sdk::input_ns::KEY_D },
    { "KEY_E", sdk::input_ns::KEY_E },
    { "KEY_F", sdk::input_ns::KEY_F },
    { "KEY_G", sdk::input_ns::KEY_G },
    { "KEY_H", sdk::input_ns::KEY_H },
    { "KEY_I", sdk::input_ns::KEY_I },
    { "KEY_J", sdk::input_ns::KEY_J },
    { "KEY_K", sdk::input_ns::KEY_K },
    { "KEY_L", sdk::input_ns::KEY_L },
    { "KEY_M", sdk::input_ns::KEY_M },
    { "KEY_N", sdk::input_ns::KEY_N },
    { "KEY_O", sdk::input_ns::KEY_O },
    { "KEY_P", sdk::input_ns::KEY_P },
    { "KEY_Q", sdk::input_ns::KEY_Q },
    { "KEY_R", sdk::input_ns::KEY_R },
    { "KEY_S", sdk::input_ns::KEY_S },
    { "KEY_T", sdk::input_ns::KEY_T },
    { "KEY_U", sdk::input_ns::KEY_U },
    { "KEY_V", sdk::input_ns::KEY_V },
    { "KEY_W", sdk::input_ns::KEY_W },
    { "KEY_X", sdk::input_ns::KEY_X },
    { "KEY_Y", sdk::input_ns::KEY_Y },
    { "KEY_Z", sdk::input_ns::KEY_Z },
    { "KEY_PAD_0", sdk::input_ns::KEY_PAD_0 },
    { "KEY_PAD_1", sdk::input_ns::KEY_PAD_1 },
    { "KEY_PAD_2", sdk::input_ns::KEY_PAD_2 },
    { "KEY_PAD_3", sdk::input_ns::KEY_PAD_3 },
    { "KEY_PAD_4", sdk::input_ns::KEY_PAD_4 },
    { "KEY_PAD_5", sdk::input_ns::KEY_PAD_5 },
    { "KEY_PAD_6", sdk::input_ns::KEY_PAD_6 },
    { "KEY_PAD_7", sdk::input_ns::KEY_PAD_7 },
    { "KEY_PAD_8", sdk::input_ns::KEY_PAD_8 },
    { "KEY_PAD_9", sdk::input_ns::KEY_PAD_9 },
    { "KEY_PAD_DIVIDE", sdk::input_ns::KEY_PAD_DIVIDE },
    { "KEY_PAD_MULTIPLY", sdk::input_ns::KEY_PAD_MULTIPLY },
    { "KEY_PAD_MINUS", sdk::input_ns::KEY_PAD_MINUS },
    { "KEY_PAD_PLUS", sdk::input_ns::KEY_PAD_PLUS },
    { "KEY_PAD_ENTER", sdk::input_ns::KEY_PAD_ENTER },
    { "KEY_PAD_DECIMAL", sdk::input_ns::KEY_PAD_DECIMAL },
    { "KEY_LBRACKET", sdk::input_ns::KEY_LBRACKET },
    { "KEY_RBRACKET", sdk::input_ns::KEY_RBRACKET },
    { "KEY_SEMICOLON", sdk::input_ns::KEY_SEMICOLON },
    { "KEY_APOSTROPHE", sdk::input_ns::KEY_APOSTROPHE },
    { "KEY_BACKQUOTE", sdk::input_ns::KEY_BACKQUOTE },
    { "KEY_COMMA", sdk::input_ns::KEY_COMMA },
    { "KEY_PERIOD", sdk::input_ns::KEY_PERIOD },
    { "KEY_SLASH", sdk::input_ns::KEY_SLASH },
    { "KEY_BACKSLASH", sdk::input_ns::KEY_BACKSLASH },
    { "KEY_MINUS", sdk::input_ns::KEY_MINUS },
    { "KEY_EQUAL", sdk::input_ns::KEY_EQUAL },
    { "KEY_ENTER", sdk::input_ns::KEY_ENTER },
    { "KEY_SPACE", sdk::input_ns::KEY_SPACE },
    { "KEY_BACKSPACE", sdk::input_ns::KEY_BACKSPACE },
    { "KEY_TAB", sdk::input_ns::KEY_TAB },
    { "KEY_CAPSLOCK", sdk::input_ns::KEY_CAPSLOCK },
    { "KEY_NUMLOCK", sdk::input_ns::KEY_NUMLOCK },
    { "KEY_ESCAPE", sdk::input_ns::KEY_ESCAPE },
    { "KEY_SCROLLLOCK", sdk::input_ns::KEY_SCROLLLOCK },
    { "KEY_INSERT", sdk::input_ns::KEY_INSERT },
    { "KEY_DELETE", sdk::input_ns::KEY_DELETE },
    { "KEY_HOME", sdk::input_ns::KEY_HOME },
    { "KEY_END", sdk::input_ns::KEY_END },
    { "KEY_PAGEUP", sdk::input_ns::KEY_PAGEUP },
    { "KEY_PAGEDOWN", sdk::input_ns::KEY_PAGEDOWN },
    { "KEY_BREAK", sdk::input_ns::KEY_BREAK },
    { "KEY_LSHIFT", sdk::input_ns::KEY_LSHIFT },
    { "KEY_RSHIFT", sdk::input_ns::KEY_RSHIFT },
    { "KEY_LALT", sdk::input_ns::KEY_LALT },
    { "KEY_RALT", sdk::input_ns::KEY_RALT },
    { "KEY_LCONTROL", sdk::input_ns::KEY_LCONTROL },
    { "KEY_RCONTROL", sdk::input_ns::KEY_RCONTROL },
    { "KEY_LWIN", sdk::input_ns::KEY_LWIN },
    { "KEY_RWIN", sdk::input_ns::KEY_RWIN },
    { "KEY_APP", sdk::input_ns::KEY_APP },
    { "KEY_UP", sdk::input_ns::KEY_UP },
    { "KEY_LEFT", sdk::input_ns::KEY_LEFT },
    { "KEY_DOWN", sdk::input_ns::KEY_DOWN },
    { "KEY_RIGHT", sdk::input_ns::KEY_RIGHT },
    { "KEY_F1", sdk::input_ns::KEY_F1 },
    { "KEY_F2", sdk::input_ns::KEY_F2 },
    { "KEY_F3", sdk::input_ns::KEY_F3 },
    { "KEY_F4", sdk::input_ns::KEY_F4 },
    { "KEY_F5", sdk::input_ns::KEY_F5 },
    { "KEY_F6", sdk::input_ns::KEY_F6 },
    { "KEY_F7", sdk::input_ns::KEY_F7 },
    { "KEY_F8", sdk::input_ns::KEY_F8 },
    { "KEY_F9", sdk::input_ns::KEY_F9 },
    { "KEY_F10", sdk::input_ns::KEY_F10 },
    { "KEY_F11", sdk::input_ns::KEY_F11 },
    { "KEY_F12", sdk::input_ns::KEY_F12 },
    { "KEY_CAPSLOCKTOGGLE", sdk::input_ns::KEY_CAPSLOCKTOGGLE },
    { "KEY_NUMLOCKTOGGLE", sdk::input_ns::KEY_NUMLOCKTOGGLE },
    { "KEY_SCROLLLOCKTOGGLE", sdk::input_ns::KEY_SCROLLLOCKTOGGLE }
};

const std::unordered_map<std::string, TimerConfigurationTimeType> timerConfigurationTimeTypeNamesToValues =
{
    { "RealTime", TimerConfigurationTimeType_RealTime },
    { "CurrentTime", TimerConfigurationTimeType_CurrentTime }
};

std::vector<unsigned char*> messageBuffers;

std::optional<sdk::input_ns::KeyCode> GetKeyCodeFromName(std::string& keyCodeName)
{
    if (keyCodeNamesToValues.count(keyCodeName) > 0)
        return keyCodeNamesToValues.at(keyCodeName);
    return std::nullopt;
}

std::optional<TimerConfigurationTimeType> GetTimerConfigurationTimeTypeFromName(std::string& timerConfigurationTimeTypeName)
{
    if (timerConfigurationTimeTypeNamesToValues.count(timerConfigurationTimeTypeName) > 0)
        return timerConfigurationTimeTypeNamesToValues.at(timerConfigurationTimeTypeName);
    return std::nullopt;
}

void LoadConfiguration()
{
    configuration = new Configuration;

    auto configurationFileHome = std::getenv(CONFIGURATION_FILE_HOME_ENVIRONMENT_VARIABLE_NAME);

    if (!configurationFileHome)
        return;

    std::string configurationFileHomeString(configurationFileHome);

    std::filesystem::path configurationFilePath(configurationFileHomeString);
    configurationFilePath.append(CONFIGURATION_FILE_NAME).concat(CONFIGURATION_FILE_EXTENSION);

    std::ifstream configurationFileInputStream(configurationFilePath);

    if (!configurationFileInputStream)
        return;

    auto configurationJSON = nlohmann::json::parse(configurationFileInputStream, nullptr, false);

    if (!configurationJSON.is_object())
        return;

    if (configurationJSON.count(JSON_CONFIGURATION_RELOAD_KEYS_KEY) > 0)
    {
        auto configurationJSONReloadKeysValue = configurationJSON.at(JSON_CONFIGURATION_RELOAD_KEYS_KEY);

        if (configurationJSONReloadKeysValue.is_array())
        {
            for (auto& configurationJSONReloadKeysArrayElement : configurationJSONReloadKeysValue)
            {
                if (configurationJSONReloadKeysArrayElement.is_string())
                {
                    auto reloadKeyName = configurationJSONReloadKeysArrayElement.get_ref<std::string&>();

                    auto reloadKeyOptional = GetKeyCodeFromName(reloadKeyName);

                    if (reloadKeyOptional.has_value())
                    {
                        configuration->reloadKeys.push_back(*reloadKeyOptional);
                    }
                }
            }
        }
    }

    if (configurationJSON.count(JSON_CONFIGURATION_TIMER_CONFIGURATIONS_CONFIGURATIONS_KEY) > 0)
    {
        auto configurationJSONTimerConfigurationsConfigurationsValue = configurationJSON.at(JSON_CONFIGURATION_TIMER_CONFIGURATIONS_CONFIGURATIONS_KEY);

        if (configurationJSONTimerConfigurationsConfigurationsValue.is_object())
        {
            for (auto& configurationJSONTimerConfigurationsConfigurationsItem : configurationJSONTimerConfigurationsConfigurationsValue.items())
            {
                const std::string& configurationJSONTimerConfigurationsConfigurationsItemKey = configurationJSONTimerConfigurationsConfigurationsItem.key();
                auto configurationJSONTimerConfigurationsConfigurationsItemValue = configurationJSONTimerConfigurationsConfigurationsItem.value();

                if (!configurationJSONTimerConfigurationsConfigurationsItemValue.is_object())
                    continue;

                if (configurationJSONTimerConfigurationsConfigurationsItemValue.count(JSON_CONFIGURATION_TIMER_CONFIGURATION_CONFIGURATION_ACTIVATE_KEY) > 0)
                {
                    auto configurationJSONTimerConfigurationConfigurationActivateValue = configurationJSONTimerConfigurationsConfigurationsItemValue.at(JSON_CONFIGURATION_TIMER_CONFIGURATION_CONFIGURATION_ACTIVATE_KEY);

                    if (configurationJSONTimerConfigurationConfigurationActivateValue.is_boolean())
                    {
                        auto timerConfigurationConfigurationActivate = configurationJSONTimerConfigurationConfigurationActivateValue.get_ref<bool&>();

                        if (!timerConfigurationConfigurationActivate)
                            continue;
                    }
                }

                std::filesystem::path timerConfigurationFilePath(configurationFileHomeString);
                timerConfigurationFilePath.append(configurationJSONTimerConfigurationsConfigurationsItemKey).append(CONFIGURATION_FILE_NAME).concat(CONFIGURATION_FILE_EXTENSION);

                std::ifstream timerConfigurationFileInputStream(timerConfigurationFilePath);

                if (!timerConfigurationFileInputStream)
                    continue;

                auto timerConfigurationJSON = nlohmann::json::parse(timerConfigurationFileInputStream, nullptr,false);

                if (!timerConfigurationJSON.is_object())
                    continue;

                if (timerConfigurationJSON.count(JSON_TIMER_CONFIGURATION_EXECUTIONS_KEY) == 0)
                    continue;

                auto timerConfigurationJSONExecutionsValue = timerConfigurationJSON.at(JSON_TIMER_CONFIGURATION_EXECUTIONS_KEY);

                if (!timerConfigurationJSONExecutionsValue.is_array())
                    continue;

                TimerConfiguration timerConfiguration;
                timerConfiguration.timeType = TimerConfigurationTimeType_RealTime;
                timerConfiguration.executeIfDead = true;
                timerConfiguration.executeIfObserver = true;

                for (auto& timerConfigurationJSONExecutionsArrayElement : timerConfigurationJSONExecutionsValue)
                {
                    if (!timerConfigurationJSONExecutionsArrayElement.is_object())
                        continue;

                    TimerConfigurationExecution timerConfigurationExecution;
                    timerConfigurationExecution.nextExecutionDelay = 0.0f;

                    if (timerConfigurationJSONExecutionsArrayElement.count(JSON_TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_KEY) > 0)
                    {
                        auto timerConfigurationJSONExecutionNetMessageValue = timerConfigurationJSONExecutionsArrayElement.at(JSON_TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_KEY);

                        if (timerConfigurationJSONExecutionNetMessageValue.is_object())
                        {
                            if (timerConfigurationJSONExecutionNetMessageValue.count(JSON_TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_NAME_KEY) > 0)
                            {
                                auto timerConfigurationJSONExecutionNetMessageNameValue = timerConfigurationJSONExecutionNetMessageValue.at(JSON_TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_NAME_KEY);

                                if (timerConfigurationJSONExecutionNetMessageNameValue.is_string())
                                {
                                    TimerConfigurationExecutionNetMessage timerConfigurationExecutionNetMessage;
                                    timerConfigurationExecutionNetMessage.reliable = true;

                                    timerConfigurationJSONExecutionNetMessageNameValue.get_to(timerConfigurationExecutionNetMessage.name);

                                    if (timerConfigurationJSONExecutionNetMessageValue.count(JSON_TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_RELIABLE_KEY) > 0)
                                    {
                                        auto timerConfigurationJSONExecutionNetMessageReliableValue = timerConfigurationJSONExecutionNetMessageValue.at(JSON_TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_RELIABLE_KEY);

                                        if (timerConfigurationJSONExecutionNetMessageReliableValue.is_boolean())
                                            timerConfigurationJSONExecutionNetMessageReliableValue.get_to(timerConfigurationExecutionNetMessage.reliable);
                                    }

                                    bool dataReadSuccess = false;
                                    if (timerConfigurationJSONExecutionNetMessageValue.count(JSON_TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_DATA_BITS_KEY) > 0)
                                    {
                                        auto timerConfigurationJSONExecutionNetMessageDataBitsValue = timerConfigurationJSONExecutionNetMessageValue.at(JSON_TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_DATA_BITS_KEY);

                                        if (timerConfigurationJSONExecutionNetMessageDataBitsValue.is_number_unsigned())
                                        {
                                            auto dataBits = timerConfigurationJSONExecutionNetMessageDataBitsValue.get_ref<std::uint64_t&>();

                                            if (dataBits > 0 && dataBits <= sdk::netchannel::clc_GMod_ClientToServer_NetMessage_Data_MaxBits)
                                            {
                                                if (timerConfigurationJSONExecutionNetMessageValue.count(JSON_TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_DATA_FILE_NAME_KEY) > 0)
                                                {
                                                    auto timerConfigurationJSONExecutionNetMessageDataFileNameValue = timerConfigurationJSONExecutionNetMessageValue.at(JSON_TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_DATA_FILE_NAME_KEY);

                                                    if (timerConfigurationJSONExecutionNetMessageDataFileNameValue.is_string())
                                                    {
                                                        auto dataFileName = timerConfigurationJSONExecutionNetMessageDataFileNameValue.get_ref<std::string&>();

                                                        std::filesystem::path dataFilePath(configurationFileHomeString);
                                                        dataFilePath.append(configurationJSONTimerConfigurationsConfigurationsItemKey).append(dataFileName).concat(TIMER_CONFIGURATION_EXECUTION_NET_MESSAGE_DATA_FILE_EXTENSION);

                                                        std::ifstream dataFileInputStream(dataFilePath, std::ios::in | std::ios::binary); //in pas oblige

                                                        if (dataFileInputStream)
                                                        {
                                                            int dataFileBufferBytes = (dataBits + 7) >> 3;

                                                            auto dataFileBuffer = new char[dataFileBufferBytes];

                                                            dataFileInputStream.read(dataFileBuffer, dataFileBufferBytes);

                                                            if (dataFileInputStream)
                                                            {
                                                                int messageBufferBits = sdk::netchannel::NET_MESSAGE_BITS + sdk::netchannel::clc_GMod_ClientToServer_Bits_Bits + (sizeof(unsigned char) << 3) + (sizeof(unsigned short) << 3) + dataBits;
                                                                int messageBufferBytes = (messageBufferBits + 7) >> 3;

                                                                auto messageBuffer = new unsigned char[messageBufferBytes];

                                                                sdk::BitBufferWriter bitBufferWriter(reinterpret_cast<unsigned int*>(messageBuffer)); //enough byte for safe cast to int?

                                                                bitBufferWriter.WriteUInt(sdk::netchannel::clc_GMod_ClientToServer, sdk::netchannel::NET_MESSAGE_BITS);
                                                                bitBufferWriter.WriteUInt((sizeof(unsigned char) << 3) + (sizeof(unsigned short) << 3) + dataBits, sdk::netchannel::clc_GMod_ClientToServer_Bits_Bits);

                                                                bitBufferWriter.WriteUInt(sdk::netchannel::clc_GMod_ClientToServer_NetMessage_Type, sizeof(unsigned char) << 3);
                                                                bitBufferWriter.WriteUInt(sdk::networkstringtable::INVALID_STRING_INDEX, sizeof(unsigned short) << 3);

                                                                bitBufferWriter.WriteBits(reinterpret_cast<unsigned char*>(dataFileBuffer), dataBits);

                                                                messageBuffers.push_back(messageBuffer);

                                                                timerConfigurationExecutionNetMessage.bitBufferWriter = bitBufferWriter;

                                                                dataReadSuccess = true;
                                                            }

                                                            delete[] dataFileBuffer;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (!dataReadSuccess)
                                    {
                                        int messageBufferBits = sdk::netchannel::NET_MESSAGE_BITS + sdk::netchannel::clc_GMod_ClientToServer_Bits_Bits + (sizeof(unsigned char) << 3) + (sizeof(unsigned short) << 3);
                                        int messageBufferBytes = (messageBufferBits + 7) >> 3;

                                        auto messageBuffer = new unsigned char[messageBufferBytes];

                                        sdk::BitBufferWriter bitBufferWriter(reinterpret_cast<unsigned int*>(messageBuffer)); //enough byte for safe cast to int?

                                        bitBufferWriter.WriteUInt(sdk::netchannel::clc_GMod_ClientToServer, sdk::netchannel::NET_MESSAGE_BITS);
                                        bitBufferWriter.WriteUInt((sizeof(unsigned char) << 3) + (sizeof(unsigned short) << 3), sdk::netchannel::clc_GMod_ClientToServer_Bits_Bits);

                                        bitBufferWriter.WriteUInt(sdk::netchannel::clc_GMod_ClientToServer_NetMessage_Type, sizeof(unsigned char) << 3);
                                        bitBufferWriter.WriteUInt(sdk::networkstringtable::INVALID_STRING_INDEX, sizeof(unsigned short) << 3);

                                        messageBuffers.push_back(messageBuffer);

                                        timerConfigurationExecutionNetMessage.bitBufferWriter = bitBufferWriter;
                                    }

                                    timerConfigurationExecution.netMessage = timerConfigurationExecutionNetMessage;
                                }
                            }
                        }
                    }

                    if (timerConfigurationJSONExecutionsArrayElement.count(JSON_TIMER_CONFIGURATION_EXECUTION_COMMAND_KEY) > 0)
                    {
                        auto timerConfigurationJSONExecutionCommandValue = timerConfigurationJSONExecutionsArrayElement.at(JSON_TIMER_CONFIGURATION_EXECUTION_COMMAND_KEY);

                        if (timerConfigurationJSONExecutionCommandValue.is_string())
                        {
                            auto command = timerConfigurationJSONExecutionCommandValue.get_ref<std::string&>();

                            timerConfigurationExecution.command = command;
                        }
                    }

                    if (timerConfigurationJSONExecutionsArrayElement.count(JSON_TIMER_CONFIGURATION_EXECUTION_NEXT_EXECUTION_DELAY_KEY) > 0)
                    {
                        auto timerConfigurationJSONExecutionNextExecutionDelayValue = timerConfigurationJSONExecutionsArrayElement.at(JSON_TIMER_CONFIGURATION_EXECUTION_NEXT_EXECUTION_DELAY_KEY);

                        if (timerConfigurationJSONExecutionNextExecutionDelayValue.is_number_float())
                        {
                            auto nextExecutionDelay = timerConfigurationJSONExecutionNextExecutionDelayValue.get<float>();

                            if (nextExecutionDelay >= 0.0f)
                                timerConfigurationExecution.nextExecutionDelay = nextExecutionDelay;
                        }
                    }

                    timerConfiguration.executions.push_back(timerConfigurationExecution);
                }

                if (timerConfiguration.executions.empty())
                    continue;

                if (timerConfigurationJSON.count(JSON_TIMER_CONFIGURATION_TIME_TYPE_KEY) > 0)
                {
                    auto timerConfigurationJSONTimeTypeValue = timerConfigurationJSON.at(JSON_TIMER_CONFIGURATION_TIME_TYPE_KEY);

                    if (timerConfigurationJSONTimeTypeValue.is_string())
                    {
                        auto timeTypeName = timerConfigurationJSONTimeTypeValue.get_ref<std::string&>();

                        auto timeTypeOptional = GetTimerConfigurationTimeTypeFromName(timeTypeName);

                        if (timeTypeOptional.has_value())
                            timerConfiguration.timeType = *timeTypeOptional;
                    }
                }

                if (timerConfigurationJSON.count(JSON_TIMER_CONFIGURATION_EXECUTE_IF_DEAD_KEY) > 0)
                {
                    auto timerConfigurationJSONExecuteIfDeadValue = timerConfigurationJSON.at(JSON_TIMER_CONFIGURATION_EXECUTE_IF_DEAD_KEY);

                    if (timerConfigurationJSONExecuteIfDeadValue.is_boolean())
                        timerConfigurationJSONExecuteIfDeadValue.get_to(timerConfiguration.executeIfDead);
                }

                if (timerConfigurationJSON.count(JSON_TIMER_CONFIGURATION_EXECUTE_IF_OBSERVER_KEY) > 0)
                {
                    auto timerConfigurationJSONExecuteIfObserverValue = timerConfigurationJSON.at(JSON_TIMER_CONFIGURATION_EXECUTE_IF_OBSERVER_KEY);

                    if (timerConfigurationJSONExecuteIfObserverValue.is_boolean())
                        timerConfigurationJSONExecuteIfObserverValue.get_to(timerConfiguration.executeIfObserver);
                }

                TimerConfigurationConfiguration timerConfigurationConfiguration;
                timerConfigurationConfiguration.loop = false;
                timerConfigurationConfiguration.automaticStart = false;

                if (configurationJSONTimerConfigurationsConfigurationsItemValue.count(JSON_CONFIGURATION_TIMER_CONFIGURATION_CONFIGURATION_START_KEYS_KEY) > 0)
                {
                    auto configurationJSONTimerConfigurationConfigurationStartKeysValue = configurationJSONTimerConfigurationsConfigurationsItemValue.at(JSON_CONFIGURATION_TIMER_CONFIGURATION_CONFIGURATION_START_KEYS_KEY);

                    if (configurationJSONTimerConfigurationConfigurationStartKeysValue.is_array())
                    {
                        for (auto& configurationJSONTimerConfigurationConfigurationStartKeysArrayElement : configurationJSONTimerConfigurationConfigurationStartKeysValue)
                        {
                            if (configurationJSONTimerConfigurationConfigurationStartKeysArrayElement.is_string())
                            {
                                auto startKeyName = configurationJSONTimerConfigurationConfigurationStartKeysArrayElement.get_ref<std::string&>();

                                auto startKeyOptional = GetKeyCodeFromName(startKeyName);

                                if (startKeyOptional.has_value())
                                    timerConfigurationConfiguration.startKeys.push_back(*startKeyOptional);
                            }
                        }
                    }
                }

                if (configurationJSONTimerConfigurationsConfigurationsItemValue.count(JSON_CONFIGURATION_TIMER_CONFIGURATION_CONFIGURATION_STOP_KEYS_KEY) > 0)
                {
                    auto configurationJSONTimerConfigurationConfigurationStopKeysValue = configurationJSONTimerConfigurationsConfigurationsItemValue.at(JSON_CONFIGURATION_TIMER_CONFIGURATION_CONFIGURATION_STOP_KEYS_KEY);

                    if (configurationJSONTimerConfigurationConfigurationStopKeysValue.is_array())
                    {
                        for (auto& configurationJSONTimerConfigurationConfigurationStopKeysArrayElement : configurationJSONTimerConfigurationConfigurationStopKeysValue)
                        {
                            if (configurationJSONTimerConfigurationConfigurationStopKeysArrayElement.is_string())
                            {
                                auto stopKeyName = configurationJSONTimerConfigurationConfigurationStopKeysArrayElement.get_ref<std::string&>();

                                auto stopKeyOptional = GetKeyCodeFromName(stopKeyName);

                                if (stopKeyOptional.has_value())
                                    timerConfigurationConfiguration.stopKeys.push_back(*stopKeyOptional);
                            }
                        }
                    }
                }

                if (configurationJSONTimerConfigurationsConfigurationsItemValue.count(JSON_CONFIGURATION_TIMER_CONFIGURATION_CONFIGURATION_LOOP_KEY) > 0)
                {
                    auto configurationJSONTimerConfigurationConfigurationLoopValue = configurationJSONTimerConfigurationsConfigurationsItemValue.at(JSON_CONFIGURATION_TIMER_CONFIGURATION_CONFIGURATION_LOOP_KEY);

                    if (configurationJSONTimerConfigurationConfigurationLoopValue.is_boolean())
                        configurationJSONTimerConfigurationConfigurationLoopValue.get_to(timerConfigurationConfiguration.loop);
                }

                if (configurationJSONTimerConfigurationsConfigurationsItemValue.count(JSON_CONFIGURATION_TIMER_CONFIGURATION_CONFIGURATION_AUTOMATIC_START_KEY) > 0)
                {
                    auto configurationJSONTimerConfigurationConfigurationAutomaticStartValue = configurationJSONTimerConfigurationsConfigurationsItemValue.at(JSON_CONFIGURATION_TIMER_CONFIGURATION_CONFIGURATION_AUTOMATIC_START_KEY);

                    if (configurationJSONTimerConfigurationConfigurationAutomaticStartValue.is_boolean())
                        configurationJSONTimerConfigurationConfigurationAutomaticStartValue.get_to(timerConfigurationConfiguration.automaticStart);
                }

                configuration->timerConfigurationsConfigurations[configurationJSONTimerConfigurationsConfigurationsItemKey] = timerConfigurationConfiguration;
                configuration->timerConfigurations[configurationJSONTimerConfigurationsConfigurationsItemKey] = timerConfiguration;
            }
        }
    }
}

void UnloadConfiguration()
{
    delete configuration;

    auto it = messageBuffers.begin();
    while (it != messageBuffers.end())
    {
        delete[] *it;

        it = messageBuffers.erase(it);
    }
}