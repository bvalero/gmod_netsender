#ifndef VIRTUALFUNCTIONGETTER_HPP
#define VIRTUALFUNCTIONGETTER_HPP

#include <cstddef>

namespace sdk::util
{

    template<typename T = void*>
    inline T GetVirtualFunction(void* object, std::size_t index)
    {
        return reinterpret_cast<T>((*reinterpret_cast<void***>(object))[index]);
    }

}

#endif //VIRTUALFUNCTIONGETTER_HPP
