#ifndef SDK_CLIENT_FRAMESTAGENOTIFY_HOOK_HPP
#define SDK_CLIENT_FRAMESTAGENOTIFY_HOOK_HPP

#include <sdk/Client.hpp>

inline sdk::client_ns::vft::function::type::NotifyFrameStage Client_FrameStageNotify = nullptr;

void __fastcall SDK_Client_FrameStageNotify_Hook(void* _this, void* edx, int currentFrameStage);

#endif //SDK_CLIENT_FRAMESTAGENOTIFY_HOOK_HPP
