#include "Think.hpp"

#include <cstddef>

#include <string>
#include <unordered_map>

#include <sdk/Engine.hpp>
#include <sdk/EngineTool.hpp>
#include <sdk/EntityList.hpp>
#include <sdk/Input.hpp>
#include <sdk/NetworkStringTableContainer.hpp>
#include <sdk/Player.hpp>
#include <filesystem>
#include <fstream>

#include "Configuration.hpp"
#include "ConfigurationLoaderUnloader.hpp"

struct TimerExecuteInformations
{

    float time;
    std::size_t index;

};

std::unordered_map<std::string, TimerExecuteInformations> timerExecuteInformations;

bool isInGameAndNotDrawingLoadingImageSave = false;

bool isReloadLastKeyDownSave = false;

std::unordered_map<std::string, bool> timerIsStartLastKeyDownSaves;
std::unordered_map<std::string, bool> timerIsStopLastKeyDownSaves;

void Think()
{
    if (!configuration->reloadKeys.empty())
    {
        auto reloadKeysCount = configuration->reloadKeys.size();

        auto reloadKeysLastIndex = reloadKeysCount - 1;

        auto reloadLastKey = configuration->reloadKeys.at(reloadKeysLastIndex);

        auto isReloadLastKeyDown = sdk::input->IsKeyDown(reloadLastKey);

        if (isReloadLastKeyDown)
        {
            if (!isReloadLastKeyDownSave)
            {
                auto areOtherReloadKeysDown = true;

                for (auto i = 0; i < reloadKeysLastIndex; i++)
                {
                    auto reloadKey = configuration->reloadKeys.at(i);

                    auto isReloadKeyDown = sdk::input->IsKeyDown(reloadKey);

                    if (!isReloadKeyDown)
                    {
                        areOtherReloadKeysDown = false;
                        break;
                    }
                }

                if (areOtherReloadKeysDown)
                {
                    timerExecuteInformations.clear();
                    isInGameAndNotDrawingLoadingImageSave = false;

                    timerIsStartLastKeyDownSaves.clear();
                    timerIsStopLastKeyDownSaves.clear();

                    UnloadConfiguration();
                    LoadConfiguration();
                }
            }
        }

        isReloadLastKeyDownSave = isReloadLastKeyDown;
    }

    if (sdk::engine->IsInGame() && !sdk::engine->IsDrawingLoadingImage())
    {
        auto realTime = sdk::engineTool->GetRealTime();
        auto currentTime = sdk::engineTool->ClientTime();

        auto localPlayerIndex = sdk::engine->GetLocalPlayer();
        auto localPlayer = reinterpret_cast<sdk::Player*>(sdk::entityList->GetClientEntity(localPlayerIndex));
        auto isLocalPlayerAlive = localPlayer->IsAlive();
        auto localPlayerObserverMode = localPlayer->GetObserverMode();

        auto networkStringTable = sdk::networkStringTableContainer->GetTable(sdk::networkstringtablecontainer_ns::networkstring_TABLE);
        auto netChannel = sdk::engine->GetNetChannelInfo();

        if (!isInGameAndNotDrawingLoadingImageSave)
        {
            for (auto& timerConfigurationConfigurationPair : configuration->timerConfigurationsConfigurations)
            {
                auto& timerName = timerConfigurationConfigurationPair.first;
                auto& timerConfigurationConfiguration = timerConfigurationConfigurationPair.second;

                auto& automaticStart = timerConfigurationConfiguration.automaticStart;

                if (automaticStart)
                {
                    auto timerConfiguration = configuration->timerConfigurations.at(timerName);
                    auto& timerConfigurationTimeType = timerConfiguration.timeType;

                    auto& time = timerConfigurationTimeType == TimerConfigurationTimeType_RealTime ? realTime : currentTime;

                    TimerExecuteInformations timerExecuteInformations_;
                    timerExecuteInformations_.time = time;
                    timerExecuteInformations_.index = 0;

                    timerExecuteInformations[timerName] = timerExecuteInformations_;
                }
            }

            isInGameAndNotDrawingLoadingImageSave = true;
        }

        for (auto& timerConfigurationConfigurationPair : configuration->timerConfigurationsConfigurations)
        {
            auto& timerName = timerConfigurationConfigurationPair.first;
            auto& timerConfigurationConfiguration = timerConfigurationConfigurationPair.second;

            if (timerIsStartLastKeyDownSaves.count(timerName) == 0)
                timerIsStartLastKeyDownSaves[timerName] = false;

            if (timerIsStopLastKeyDownSaves.count(timerName) == 0)
                timerIsStopLastKeyDownSaves[timerName] = false;

            if (timerExecuteInformations.count(timerName) > 0)
            {
                if (!timerConfigurationConfiguration.stopKeys.empty())
                {
                    auto stopKeysCount = timerConfigurationConfiguration.stopKeys.size();

                    auto stopKeysLastIndex = stopKeysCount - 1;

                    auto stopLastKey = timerConfigurationConfiguration.stopKeys.at(stopKeysLastIndex);

                    auto isStopLastKeyDown = sdk::input->IsKeyDown(stopLastKey);

                    if (isStopLastKeyDown)
                    {
                        if (!timerIsStopLastKeyDownSaves.at(timerName))
                        {
                            auto areOtherStopKeysDown = true;

                            for (auto i = 0; i < stopKeysLastIndex; i++)
                            {
                                auto stopKey = timerConfigurationConfiguration.stopKeys.at(i);

                                auto isStopKeyDown = sdk::input->IsKeyDown(stopKey);

                                if (!isStopKeyDown)
                                {
                                    areOtherStopKeysDown = false;
                                    break;
                                }
                            }

                            if (areOtherStopKeysDown)
                            {
                                timerExecuteInformations.erase(timerName);
                            }
                        }
                    }

                    timerIsStopLastKeyDownSaves[timerName] = isStopLastKeyDown;
                }
            }
            else
            {
                if (!timerConfigurationConfiguration.startKeys.empty())
                {
                    auto startKeysCount = timerConfigurationConfiguration.startKeys.size();

                    auto startKeysLastIndex = startKeysCount - 1;

                    auto startLastKey = timerConfigurationConfiguration.startKeys.at(startKeysLastIndex);

                    auto isStartLastKeyDown = sdk::input->IsKeyDown(startLastKey);

                    if (isStartLastKeyDown)
                    {
                        if (!timerIsStartLastKeyDownSaves.at(timerName))
                        {
                            auto areOtherStartKeysDown = true;

                            for (auto i = 0; i < startKeysLastIndex; i++)
                            {
                                auto startKey = timerConfigurationConfiguration.startKeys.at(i);

                                auto isStartKeyDown = sdk::input->IsKeyDown(startKey);

                                if (!isStartKeyDown)
                                {
                                    areOtherStartKeysDown = false;
                                    break;
                                }
                            }

                            if (areOtherStartKeysDown)
                            {
                                auto timerConfiguration = configuration->timerConfigurations.at(timerName);
                                auto& timerConfigurationTimeType = timerConfiguration.timeType;

                                auto& time = timerConfigurationTimeType == TimerConfigurationTimeType_RealTime ? realTime : currentTime;

                                TimerExecuteInformations timerExecuteInformations_;
                                timerExecuteInformations_.time = time;
                                timerExecuteInformations_.index = 0;

                                timerExecuteInformations[timerName] = timerExecuteInformations_;
                            }
                        }
                    }

                    timerIsStartLastKeyDownSaves[timerName] = isStartLastKeyDown;
                }
            }
        }

        auto it = timerExecuteInformations.begin();
        while (it != timerExecuteInformations.end())
        {
            auto& timerExecuteInformationsPair = *it;

            auto& timerName = timerExecuteInformationsPair.first;
            auto& timerExecuteInformations_ = timerExecuteInformationsPair.second;

            auto& timerExecuteTime = timerExecuteInformations_.time;
            auto& timerExecuteIndex = timerExecuteInformations_.index;

            auto timerConfiguration = configuration->timerConfigurations.at(timerName);
            auto& timerConfigurationTimeType = timerConfiguration.timeType;
            auto& timerConfigurationExecuteIfDead = timerConfiguration.executeIfDead;
            auto& timerConfigurationExecuteIfObserver = timerConfiguration.executeIfObserver;

            auto& time = timerConfigurationTimeType == TimerConfigurationTimeType_RealTime ? realTime : currentTime;

            if (time > timerExecuteTime)
            {
                if ((isLocalPlayerAlive || timerConfigurationExecuteIfDead) && (localPlayerObserverMode == sdk::player::OBS_MODE_NONE || timerConfigurationExecuteIfObserver))
                {
                    auto timerConfigurationExecution = timerConfiguration.executions.at(timerExecuteIndex);

                    auto& timerConfigurationExecutionNetMessageOptional = timerConfigurationExecution.netMessage;
                    auto& timerConfigurationExecutionCommandOptional = timerConfigurationExecution.command;
                    auto& timerConfigurationExecutionNextExecutionDelay = timerConfigurationExecution.nextExecutionDelay;

                    if (timerConfigurationExecutionNetMessageOptional.has_value())
                    {
                        auto timerConfigurationExecutionNetMessage = *timerConfigurationExecutionNetMessageOptional;

                        auto& netMessageName = timerConfigurationExecutionNetMessage.name;
                        auto& netMessageReliable = timerConfigurationExecutionNetMessage.reliable;
                        auto& netMessageBitBufferWriter = timerConfigurationExecutionNetMessage.bitBufferWriter;

                        auto netMessageID = networkStringTable->FindStringIndex(netMessageName.c_str());

                        if (netMessageID != sdk::networkstringtable::INVALID_STRING_INDEX)
                        {
                            auto netMessageBitBufferWriterEndSave = netMessageBitBufferWriter.position;

                            netMessageBitBufferWriter.SetPosition(sdk::netchannel::NET_MESSAGE_BITS + sdk::netchannel::clc_GMod_ClientToServer_Bits_Bits + (sizeof(unsigned char) << 3));

                            netMessageBitBufferWriter.WriteUInt(netMessageID, sizeof(unsigned short) << 3);

                            netMessageBitBufferWriter.SetPosition(netMessageBitBufferWriterEndSave);

                            netChannel->SendData(netMessageBitBufferWriter, netMessageReliable);

                            netMessageBitBufferWriter.SetPosition(sdk::netchannel::NET_MESSAGE_BITS + sdk::netchannel::clc_GMod_ClientToServer_Bits_Bits + (sizeof(unsigned char) << 3));

                            netMessageBitBufferWriter.WriteUInt(sdk::networkstringtable::INVALID_STRING_INDEX, sizeof(unsigned short) << 3);

                            netMessageBitBufferWriter.SetPosition(netMessageBitBufferWriterEndSave);
                        }
                    }

                    if (timerConfigurationExecutionCommandOptional.has_value())
                    {
                        auto timerConfigurationExecutionCommand = *timerConfigurationExecutionCommandOptional;

                        sdk::engine->ClientCmd_Unrestricted(timerConfigurationExecutionCommand.c_str());
                    }

                    if (timerConfiguration.executions.size() - 1 != timerExecuteIndex)
                    {
                        timerExecuteTime = time + timerConfigurationExecutionNextExecutionDelay;
                        timerExecuteIndex++;
                    }
                    else
                    {
                        auto timerConfigurationConfiguration = configuration->timerConfigurationsConfigurations.at(timerName);

                        auto& loop = timerConfigurationConfiguration.loop;

                        if (loop)
                        {
                            timerExecuteTime = time + timerConfigurationExecutionNextExecutionDelay;
                            timerExecuteIndex = 0;
                        }
                        else
                        {
                            it = timerExecuteInformations.erase(it);
                            continue;
                        }
                    }
                }
            }

            it++;
        }
    }
    else
    {
        if (isInGameAndNotDrawingLoadingImageSave)
        {
            timerExecuteInformations.clear();

            isInGameAndNotDrawingLoadingImageSave = false;
        }
    }
}