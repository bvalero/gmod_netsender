#include "SDK_Interfaces.hpp"

#include <Windows.h>

#include <sdk/Client.hpp>
#include <sdk/Engine.hpp>
#include <sdk/EntityList.hpp>
#include <sdk/EngineTool.hpp>
#include <sdk/Input.hpp>
#include <sdk/NetworkStringTableContainer.hpp>

constexpr char CreateInterface_NAME[] = "CreateInterface";

constexpr char ENGINE_LIBRARY[] = "engine.dll";
constexpr char CLIENT_LIBRARY[] = "client.dll";
constexpr char VGUI2_LIBRARY[] = "vgui2.dll";

constexpr char VENGINE_CLIENT_INTERFACE_VERSION[] = "VEngineClient015";
constexpr char VENGINETOOL_INTERFACE_VERSION[] = "VENGINETOOL003";
constexpr char INTERFACENAME_NETWORKSTRINGTABLECLIENT[] = "VEngineClientStringTable001";
constexpr char CLIENT_DLL_INTERFACE_VERSION[] = "VClient017";
constexpr char VCLIENTENTITYLIST_INTERFACE_VERSION[] = "VClientEntityList003";
constexpr char VGUI_INPUT_INTERFACE_VERSION[] = "VGUI_Input005";

using CreateInterface_t = void* (*) (const char* pName, int* pReturnCode);

CreateInterface_t GetCreateInterfaceFunction(const char* library)
{
    HMODULE handle = GetModuleHandleA(library);

    return reinterpret_cast<CreateInterface_t>(GetProcAddress(handle, CreateInterface_NAME));
}

void CreateSDKInterfaces()
{
    CreateInterface_t Engine_CreateInterface = GetCreateInterfaceFunction(ENGINE_LIBRARY);

    sdk::engine = reinterpret_cast<sdk::Engine*>(Engine_CreateInterface(VENGINE_CLIENT_INTERFACE_VERSION, nullptr));

    sdk::engineTool = reinterpret_cast<sdk::EngineTool*>(Engine_CreateInterface(VENGINETOOL_INTERFACE_VERSION, nullptr));

    sdk::networkStringTableContainer = reinterpret_cast<sdk::NetworkStringTableContainer*>(Engine_CreateInterface(INTERFACENAME_NETWORKSTRINGTABLECLIENT, nullptr));

    CreateInterface_t Client_CreateInterface = GetCreateInterfaceFunction(CLIENT_LIBRARY);

    sdk::client = reinterpret_cast<sdk::Client*>(Client_CreateInterface(CLIENT_DLL_INTERFACE_VERSION, nullptr));

    sdk::entityList = reinterpret_cast<sdk::EntityList*>(Client_CreateInterface(VCLIENTENTITYLIST_INTERFACE_VERSION, nullptr));

    CreateInterface_t VGUI2_CreateInterface = GetCreateInterfaceFunction(VGUI2_LIBRARY);

    sdk::input = reinterpret_cast<sdk::Input*>(VGUI2_CreateInterface(VGUI_INPUT_INTERFACE_VERSION, nullptr));
    
}
