#ifndef NETWORKSTRINGTABLE_HPP
#define NETWORKSTRINGTABLE_HPP

#include "util/VirtualFunctionGetter.hpp"

namespace sdk
{

    namespace networkstringtable
    {

        inline constexpr unsigned short INVALID_STRING_INDEX { static_cast<unsigned short>(-1) };

        namespace vft::function
        {

            namespace index
            {

                inline constexpr std::size_t FindStringIndex { 12 };

            }

            namespace type
            {

                using FindStringIndex = int (__thiscall*) (void* _this, char const* string);

            }

        }

    }

    class NetworkStringTable
    {

    public:

        int FindStringIndex(char const* string)
        {
            return util::GetVirtualFunction<networkstringtable::vft::function::type::FindStringIndex>(this, networkstringtable::vft::function::index::FindStringIndex) (this, string);
        }

    };

}

#endif //NETWORKSTRINGTABLE_HPP
