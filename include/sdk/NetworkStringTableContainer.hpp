#ifndef NETWORKSTRINGTABLECONTAINER_HPP
#define NETWORKSTRINGTABLECONTAINER_HPP

#include "util/VirtualFunctionGetter.hpp"

#include "NetworkStringTable.hpp"

namespace sdk
{

    namespace networkstringtablecontainer_ns
    {

        inline constexpr int networkstring_TABLE { 16 }; //namespace table?

        namespace vft::function
        {

            namespace index
            {

                inline constexpr std::size_t GetTable { 4 };

            }

            namespace type
            {

                using GetTable = NetworkStringTable* (__thiscall*) (void* _this, int stringTable);

            }

        }

    }

    class NetworkStringTableContainer
    {

    public:

        NetworkStringTable* GetTable(int stringTable)
        {
            return util::GetVirtualFunction<networkstringtablecontainer_ns::vft::function::type::GetTable>(this, networkstringtablecontainer_ns::vft::function::index::GetTable) (this, stringTable);
        }

    };

    inline NetworkStringTableContainer* networkStringTableContainer { nullptr };

}

#endif //NETWORKSTRINGTABLECONTAINER_HPP
