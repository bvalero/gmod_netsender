#include "SDK_Client_FrameStageNotify_Hook.hpp"

#include "../../../Think.hpp"

void __fastcall SDK_Client_FrameStageNotify_Hook(void* _this, void* edx, int currentFrameStage)
{
    if (currentFrameStage == static_cast<int>(sdk::client_ns::FrameStage::RENDER_START))
    {
        Think();
    }

    Client_FrameStageNotify(_this, currentFrameStage);
}