#ifndef ENTITY_HPP
#define ENTITY_HPP

#include "util/VirtualFunctionGetter.hpp"

namespace sdk
{

    namespace entity
    {

        namespace vft::function
        {

            namespace index
            {

                inline constexpr std::size_t IsAlive { 129 };

            }

            namespace type
            {

                using IsAlive = bool (__thiscall*) (void* _this);

            }

        }

    }

    class Entity
    {

    public:

        bool IsAlive()
        {
            return util::GetVirtualFunction<entity::vft::function::type::IsAlive>(this, entity::vft::function::index::IsAlive) (this);
        }

    };

}

#endif //ENTITY_HPP
