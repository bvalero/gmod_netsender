#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP

#include <string>
#include <vector>
#include <unordered_map>
#include <optional>

#include <sdk/BitBuffer.hpp>
#include <sdk/Input.hpp>

struct TimerConfigurationExecutionNetMessage
{

    std::string name;
    bool reliable;
    sdk::BitBufferWriter bitBufferWriter;

};

struct TimerConfigurationExecution
{

    std::optional<TimerConfigurationExecutionNetMessage> netMessage;
    std::optional<std::string> command;
    float nextExecutionDelay;

};

enum TimerConfigurationTimeType
{

    TimerConfigurationTimeType_RealTime,
    TimerConfigurationTimeType_CurrentTime,

};

struct TimerConfiguration
{

    TimerConfigurationTimeType timeType;
    bool executeIfDead;
    bool executeIfObserver;
    std::vector<TimerConfigurationExecution> executions;

};

struct TimerConfigurationConfiguration
{

    std::vector<sdk::input_ns::KeyCode> startKeys;
    std::vector<sdk::input_ns::KeyCode> stopKeys;
    bool loop;
    bool automaticStart;

};

struct Configuration
{

    std::vector<sdk::input_ns::KeyCode> reloadKeys;
    std::unordered_map<std::string, TimerConfigurationConfiguration> timerConfigurationsConfigurations;
    std::unordered_map<std::string, TimerConfiguration> timerConfigurations;

};

inline Configuration* configuration = nullptr;

#endif //CONFIGURATION_HPP
