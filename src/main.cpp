#include "ConfigurationLoaderUnloader.hpp"
#include "sdk/interface/SDK_Interfaces.hpp"
#include "sdk/hook/SDK_Hooks.hpp"

#include <Windows.h>

void Init()
{
    LoadConfiguration();

    CreateSDKInterfaces();

    CreateSDKHooks();
}

void Shutdown()
{
    DestroySDKHooks();

    UnloadConfiguration();
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD reason, LPVOID lpReserved)
{
    if (reason == DLL_PROCESS_ATTACH)
    {
        Init();
    }

    else if (reason == DLL_PROCESS_DETACH)
    {
        Shutdown();
    }

    return TRUE;
}