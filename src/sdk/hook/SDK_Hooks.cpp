#include "SDK_Hooks.hpp"

#include <hook/VFTHooker.hpp>

#include <sdk/Client.hpp>

#include "client/SDK_Client_FrameStageNotify_Hook.hpp"

hook::VFTHooker* clientVMTHooker = nullptr;

void CreateSDKHooks()
{
    clientVMTHooker = new hook::VFTHooker(sdk::client);

    Client_FrameStageNotify = clientVMTHooker->GetOriginalFunction<sdk::client_ns::vft::function::type::NotifyFrameStage>(sdk::client_ns::vft::function::index::NotifyFrameStage);

    clientVMTHooker->SetNewVFT();
    clientVMTHooker->SetNewVFTFunction(sdk::client_ns::vft::function::index::NotifyFrameStage, reinterpret_cast<void*>(SDK_Client_FrameStageNotify_Hook));
}

void DestroySDKHooks()
{
    clientVMTHooker->SetOriginalVFT();

    delete clientVMTHooker;

    Client_FrameStageNotify = nullptr;
}
