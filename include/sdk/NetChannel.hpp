#ifndef NETCHANNEL_HPP
#define NETCHANNEL_HPP

#include "util/VirtualFunctionGetter.hpp"

#include "BitBuffer.hpp"

namespace sdk
{

    namespace netchannel
    {

        inline constexpr int NET_MESSAGE_BITS { 6 };

        inline constexpr unsigned int clc_GMod_ClientToServer { 18 };
        inline constexpr int clc_GMod_ClientToServer_Bits_Bits { 20 };
        inline constexpr unsigned int clc_GMod_ClientToServer_NetMessage_Type { 0 };

        inline constexpr unsigned int clc_GMod_ClientToServer_NetMessage_Data_MaxBits { 524264 };

        namespace vft::function
        {

            namespace index
            {

                inline constexpr std::size_t SendData { 41 };

            }

            namespace type
            {

                using SendData = bool (__thiscall*) (void* _this, BitBufferWriter& msg, bool bReliable);

            }

        }

    }

    class NetChannel
    {

    public:

        bool SendData(BitBufferWriter& msg, bool bReliable)
        {
            return util::GetVirtualFunction<netchannel::vft::function::type::SendData>(this, netchannel::vft::function::index::SendData) (this, msg, bReliable);
        }

    };

}

#endif //NETCHANNEL_HPP
