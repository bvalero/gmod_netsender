#ifndef VFTHOOKER_HPP
#define VFTHOOKER_HPP

#include <cstddef>

namespace hook
{

    class VFTHooker
    {

        void*** VFTPointer { nullptr };

        void** originalVFT { nullptr };
        void** newVFT { nullptr };

    public:

        VFTHooker(void* object) : VFTPointer { reinterpret_cast<void***>(object) }, originalVFT { *VFTPointer }
        {

        }

        ~VFTHooker()
        {
            delete[] newVFT;
        }

        void SetNewVFT()
        {
            if (!newVFT)
            {
                std::size_t size { 0 };
                while (originalVFT[size]) ++size;

                newVFT = new void*[size];

                for (std::size_t i { 0 }; i < size; ++i)
                    newVFT[i] = originalVFT[i];
            }

            *VFTPointer = newVFT;
        }

        void SetNewVFTFunction(std::size_t index, void* function)
        {
            newVFT[index] = function;
        }

        void SetNewVFTOriginalFunction(std::size_t index)
        {
            newVFT[index] = GetOriginalFunction(index);
        }

        void SetOriginalVFT()
        {
            *VFTPointer = originalVFT;
        }

        template<typename T = void*>
        T GetOriginalFunction(std::size_t index)
        {
            return reinterpret_cast<T>(originalVFT[index]);
        }

    };

}

#endif //VFTHOOKER_HPP
