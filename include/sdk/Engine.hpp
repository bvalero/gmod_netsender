#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "util/VirtualFunctionGetter.hpp"

#include "NetChannel.hpp"

namespace sdk
{

    namespace engine_ns
    {

        namespace vft::function
        {

            namespace index
            {

                inline constexpr std::size_t GetLocalPlayer { 12 };
                inline constexpr std::size_t IsInGame { 26 };
                inline constexpr std::size_t IsDrawingLoadingImage { 28 };
                inline constexpr std::size_t GetNetChannelInfo { 72 };
                inline constexpr std::size_t ClientCmd_Unrestricted { 106 };

            }

            namespace type
            {

                using GetLocalPlayer = int (__thiscall*) (void* _this);
                using IsInGame = bool (__thiscall*) (void* _this);
                using IsDrawingLoadingImage = bool (__thiscall*) (void* _this);
                using GetNetChannelInfo = NetChannel* (__thiscall*) (void* _this);
                using ClientCmd_Unrestricted = void (__thiscall*) (void* _this, const char* szCmdString);

            }

        }

    }

    class Engine
    {

    public:

        int GetLocalPlayer()
        {
            return util::GetVirtualFunction<engine_ns::vft::function::type::GetLocalPlayer>(this, engine_ns::vft::function::index::GetLocalPlayer) (this);
        }

        bool IsInGame()
        {
            return util::GetVirtualFunction<engine_ns::vft::function::type::IsInGame>(this, engine_ns::vft::function::index::IsInGame) (this);
        }

        bool IsDrawingLoadingImage()
        {
            return util::GetVirtualFunction<engine_ns::vft::function::type::IsDrawingLoadingImage>(this, engine_ns::vft::function::index::IsDrawingLoadingImage) (this);
        }

        NetChannel* GetNetChannelInfo()
        {
            return util::GetVirtualFunction<engine_ns::vft::function::type::GetNetChannelInfo>(this, engine_ns::vft::function::index::GetNetChannelInfo) (this);
        }

        void ClientCmd_Unrestricted(const char* szCmdString)
        {
            return util::GetVirtualFunction<engine_ns::vft::function::type::ClientCmd_Unrestricted>(this, engine_ns::vft::function::index::ClientCmd_Unrestricted) (this, szCmdString);
        }

    };

    inline Engine* engine { nullptr };

}

#endif //ENGINE_HPP
