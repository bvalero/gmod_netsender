#ifndef ENTITYLIST_HPP
#define ENTITYLIST_HPP

#include "util/VirtualFunctionGetter.hpp"

#include "Entity.hpp"

namespace sdk
{

    namespace entitylist_ns
    {

        namespace vft::function
        {

            namespace index
            {

                inline constexpr std::size_t GetClientEntity { 3 };

            }

            namespace type
            {

                using GetClientEntity = Entity* (__thiscall*) (void* _this, int entnum);

            }

        }

    }

    class EntityList
    {

    public:

        Entity* GetClientEntity(int entnum)
        {
            return util::GetVirtualFunction<entitylist_ns::vft::function::type::GetClientEntity>(this, entitylist_ns::vft::function::index::GetClientEntity) (this, entnum);
        }

    };

    inline EntityList* entityList { nullptr };

}

#endif //ENTITYLIST_HPP
