#ifndef ENGINETOOL_HPP
#define ENGINETOOL_HPP

#include "util/VirtualFunctionGetter.hpp"

namespace sdk
{

    namespace enginetool_ns
    {

        namespace vft::function
        {

            namespace index
            {

                inline constexpr std::size_t GetRealTime { 34 };
                inline constexpr std::size_t ClientTime { 45 };

            }

            namespace type
            {

                using GetRealTime = float (__thiscall*) (void* _this);
                using ClientTime = float (__thiscall*) (void* _this);

            }

        }

    }

    class EngineTool
    {

    public:

        float GetRealTime()
        {
            return util::GetVirtualFunction<enginetool_ns::vft::function::type::GetRealTime>(this, enginetool_ns::vft::function::index::GetRealTime) (this);
        }

        float ClientTime()
        {
            return util::GetVirtualFunction<enginetool_ns::vft::function::type::ClientTime>(this, enginetool_ns::vft::function::index::ClientTime) (this);
        }

    };

    inline EngineTool* engineTool { nullptr };

}

#endif //ENGINETOOL_HPP
